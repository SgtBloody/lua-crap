local args = { "d", "i", "n", "g", "u", "s" }

local function removeFirstArg(...)
    local t = table.pack(...)
    table.remove(t, 1)
    return t
end

local function removeLastArg(...)
    local t = table.pack(...)
    table.remove(t, #t)
    return t
end

local function printTable(t)
    for index, value in ipairs(t) do
        io.write(index, " = ", value, "\n")
    end
end

printTable(arg)
io.write "\n"
printTable(removeFirstArg(table.unpack(arg)))
io.write "\n"
printTable(removeLastArg(table.unpack(arg)))
