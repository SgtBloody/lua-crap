local function readNumber()
    local x = tonumber(io.read())

    while type(x) ~= "number" do
        io.write("Insert number!: ")
        x = tonumber(io.read())
    end
    return x
end

local function read(minimum, maximum)
    local x, y

    -- Read 1st number
    while not x or x >= maximum or x < minimum do
        io.write("Insert number (", minimum, "-", maximum - 1, "): ")
        x = readNumber()
    end

    -- Read 2nd number
    while not y or y <= x or y > maximum do
        io.write("Insert number (", x + 1, "-", maximum, "): ")
        y = readNumber()
    end
    return x, y
end

local function readHowManyNumbers()
    io.write("How many numbers do you want to generate?: ")
    return readNumber()
end

local function generateNumbers(firstNumber, secondNumber, count)
    io.write("\nRange: ", firstNumber, "-", secondNumber, "\n-----------\n")

    for i = 1, count do
        local z = math.random(firstNumber, secondNumber)
        io.write("Number ", i, " = ", z, "\n")
    end

    if count == 1
    then
        io.write("\nGenerated ", count, " number\n")
    else
        io.write("\nGenerated ", count, " numbers\n")
    end
end

local maxRange = {
    min = 0,
    max = 100
}

local x, y = read(maxRange.min, maxRange.max)
generateNumbers(x, y, readHowManyNumbers())
