local function remove(string, rsBegin, nOfCharacters)
    if nOfCharacters < 1
    then
        return "error: cannot remove 0 or negative number of characters"
    elseif nOfCharacters >= #string
    then
        return string.sub(string, 1, rsBegin)
    elseif rsBegin == 1
    then
        return string.sub(string, nOfCharacters + 1, -1)
    else
        return string.sub(string, 1, rsBegin - 1) .. string.sub(string, rsBegin + nOfCharacters, -1)
    end
end

local s = "dingus"

io.write("string = ", s, "\n\n")

print(remove(s, 4, 6))
print(remove(s, 1, 3))
print(remove(s, 2, 2))
print(remove(s, -5, 0))
