local function createArray(j, n)
    local a = {}
    for i = j, n do
        a[i] = math.random(j, n)
    end
    return a
end

local function printArray(t)
    return io.write(table.unpack(t))
end

local function printArray2(t)
    for index, value in ipairs(t) do
        io.write(index, " = ", value, "\n")
    end
end

printArray2(createArray(1,100000))
