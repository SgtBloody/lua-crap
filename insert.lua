local function insert(string, position, stringIn)
    if position == 1 then
        return stringIn .. string
    elseif position == -1 or position == #string then
        return string .. stringIn
    else
        return string.sub(string, 1, position) .. stringIn .. string.sub(string, position + 1, -1)
    end
end

local s = "dingus"
print(insert(s, 1, "dank-"))
